<?php

require_once(getcwd() . '/autoloader.php');

$logger = new \Core\Model\Log();

try {
    $reader = new \Remote\Controller();
    $view   = new \Remote\View\Rss();
    $json   = $reader->execute();
    $feeds  = json_decode($reader->execute(), true);
    $output = $view->presentAll($feeds);

    $view->printText($output);

} catch (\Error $error) {
    $logger->error($error);
} catch (\Exception $exception) {
    $logger->error($exception);
}
