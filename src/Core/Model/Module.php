<?php


namespace Core\Model;


use Core\Exceptions\NoRouteException;

class Module
{
    /**
     * Will attempt to match path and run appropriate controller.
     *
     * @param string $path
     * @return string|null
     * @throws NoRouteException
     */
    public function matchPath(string $path): ?string
    {
        $modules        = $this->getModules();
        $controllers    = $this->getControllers($modules);
        $controller     = $this->findController($path, $controllers);

        if (!$controller) {
            throw new NoRouteException('Route ' . $path . ' does not exist.');
        }

        $classPath  = str_replace('/', '\\', $controller);
        $controller = new $classPath();

        return $controller->execute();
    }

    public function findController(string $path, array $controllers, ?string $cwd = null): ?string
    {
        if (!$cwd) {
            $cwd = getcwd();
        }

        foreach ($controllers as $controller) {
            $controllerPath = $this->formatPath($controller, $cwd);
            if ($this->checkMatch($path, $controllerPath)) {
                return str_replace($cwd, '', $controller);
            }
        }

        return null;
    }

    protected function checkMatch(string $path, string $controllerPath): bool
    {
        if (substr($path, -1) == '/') {
            $path = preg_replace('{/$}', '', $path);
        }

        if (substr($controllerPath, -1) == '/') {
            $controllerPath = preg_replace('{/$}', '', $path);
        }

        return ($path === $controllerPath);
    }

    protected function formatPath(string $path, string $remove)
    {
        $path = str_replace($remove, '', $path);
        $path = str_replace('Controller', '', $path);
        $path = strtolower($path);

        return $path;
    }

    public function getModules()
    {
        $ignore = [
            '.',
            '..',
            'autoloader.php',
            'bin',
            'index.php',
        ];
        $modules = [];
        $files = scandir(getcwd());
        foreach ($files as $file) {
            if (in_array($file, $ignore)) {
                continue;
            }

            $modules[] = $file;
        }

        return $modules;
    }

    public function getControllers(array $modules, ?array $ignore = null, ?string $path = null): array
    {
        if ($ignore === null) {
            $ignore = [
                '.',
                '..',
            ];
        }

        $controllers = [];
        foreach ($modules as $module) {
            $path           = getcwd() . '/' . $module;
            $matching       = $this->returnMatching($path, true, $ignore, 'Controller', true);
            $controllers    = array_merge($controllers, $matching);
        }

        return $controllers;
    }

    public function returnMatching(
        string $path = null,
        bool $recursive = false,
        array $ignore = null,
        string $criteria = null,
        bool $removeEnding = false
    ): array
    {
        if (!$path) {
            $path = getcwd();
        }

        if ($ignore === null) {
            $ignore = [
                '.',
                '..',
            ];
        }

        $results        = [];
        $folderContents = scandir($path);
        foreach ($folderContents as $item) {
            if (in_array($item, $ignore)) {
                continue;
            }

            $fullPath = $path . '/' . $item;
            if (is_dir($fullPath) && $recursive === true) {
                $subFolder  = $this->processFolder($fullPath, $recursive, $ignore, $criteria, $removeEnding);
                $results    = array_merge($results, $subFolder);

                continue;
            }

            if (is_file($fullPath) && $removeEnding === true) {
                $item       = pathinfo($item, PATHINFO_FILENAME);
                $fullPath   = $path . '/' . $item;
            }

            if ($criteria && $item !== $criteria) {
                continue; // doesnt match
            }

            $results[] = $fullPath;
        }

        return $results;
    }

    protected function processFolder(
        string $path,
        bool $recursive,
        array $ignore,
        ?string $criteria,
        bool $removeEnding,
        int $depth = 0
    ): array
    {
        $depth++;
        if ($depth > 50) {
            return []; // Safety "switch" for the recursion
        }

        $result = [];
        $items  = scandir($path);
        foreach ($items as $item) {
            if (in_array($item, $ignore)) {
                continue;
            }

            $fullPath = $path . '/' . $item;
            if (is_dir($fullPath) && $recursive === true) {
                $subFolder  = $this->processFolder($fullPath, $recursive, $ignore, $criteria, $removeEnding, $depth);
                $result     = array_merge($result, $subFolder);

                continue;
            }

            if (is_dir($fullPath)) {
                continue; // skip sub folders, dont add folder to result either
            }

            // We assume its a file now

            if ($criteria && $criteria !== $item) {
                continue; // didnt match
            }

            if (is_file($fullPath) && $removeEnding === true) {
                $fullPath = $path . '/' . pathinfo($item, PATHINFO_FILENAME);
            }

            $result[] = $fullPath;
        }

        return $result;
    }
}
