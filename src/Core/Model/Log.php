<?php


namespace Core\Model;


class Log
{
    protected function varPath(): string
    {
        $path = getcwd() . '/../var';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }

    protected function getFile(string $name = 'error'): string
    {
        $var        = $this->varPath();
        $logPath    = $var . '/log';

        if (!file_exists($logPath)) {
            mkdir($logPath, 0777, true);
        }

        $logFile = $logPath . '/' . $name . '.log';
        if (file_exists($logFile)) {
            $f = fopen($logFile, 'w');
            fclose($f);
        }

        return $logFile;
    }

    public function error($error): bool
    {
        $message = $error . PHP_EOL . PHP_EOL;
        return error_log($message, 3, $this->getFile('error'));
    }
}
