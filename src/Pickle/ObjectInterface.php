<?php

namespace Pickle;

interface ObjectInterface
{
    public function getName(): ?string;
    public function getIsModified(): bool;
}
