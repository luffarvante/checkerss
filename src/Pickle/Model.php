<?php


namespace Pickle;


class Model
{
    public function create(?ObjectInterface $object): ?ObjectInterface
    {
        $file = $this->getFileNameFromObject($object);
        $data = serialize($object);

        $this->createDirs($file);
        $this->createFile($file);
        file_put_contents($file, $data);

        return $object;
    }



    public function read(string $name, string $type): ?ObjectInterface
    {
        $file   = $this->getFileName($name, $type);
        $data   = file_get_contents($file);
        $object = unserialize($data);
        if (!$object) {
            return null;
        }

        return $object;
    }

    public function readAll(string $type): array
    {
        $folder = $this->getFolderName($type);
        $files  = scandir($folder);

        $objects = [];
        foreach ($files as $file) {
            $path = sprintf('%s/%s', $folder, $file);
            if (is_dir($path)) {
                continue;
            }

            $data   = file_get_contents($path);
            $object = unserialize($data);

            if (!$object) {
                continue;
            }

            $objects[] = $object;
        }

        return  $objects;
    }

    public function update(?ObjectInterface $object): ?ObjectInterface
    {
        $this->delete($object);

        return $this->create($object);
    }

    public function delete(?ObjectInterface $object): bool
    {
        $file = $this->getFileNameFromObject($object);

        if (!file_exists($file)) {
            return false;
        }

        unlink($file);

        return true;
    }

    private function getFileNameFromObject(ObjectInterface $object)
    {
        $name = $object->getName();
        $type = get_class($object);

        return $this->getFileName($name, $type);
    }
    private function getFileName(string $name, string $type): string
    {
        $name   = preg_replace('/\\\/', '_', $name);
        $folder = $this->getFolderName($type);
        $file   = sprintf('%s/%s.phickle', $folder, $name);

        $this->createDirs($folder);
        $this->createFile($file);

        return $file;
    }

    private function getFolderName(string $type): string
    {
        $type   = preg_replace('/\\\/', '_', $type);
        $base   = sprintf('%s/../resources', getcwd());
        $folder = sprintf('%s/%s', $base, $type);

        return  $folder;
    }

    private function createFile(string $name): void
    {
        if (file_exists($name)) {
            return;
        }

        $file = fopen($name, 'w');
        fclose($file);
    }

    private function createDirs(string $path): void
    {
        if (file_exists($path) || is_dir($path)) {
            return;
        }

        mkdir($path, 0777, true);
    }
}
