# Pickle
Concept based on the pickle package for Python.

Allows for saving objects to text files with the `.phickle` ending.
It's used for the basic persistence of the framework.
