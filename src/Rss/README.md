# Rss
Any new Rss-related functionality should go in this module.

The plan is to migrate the Remote module here, as the module name / namespace
better reflects the module purpose.
