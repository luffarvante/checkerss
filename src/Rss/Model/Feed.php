<?php


namespace Rss\Model;


use Rss\Exceptions\InvalidSortOptionException;
use Rss\Exceptions\InvalidListException;

class Feed
{
    /**
     * @throws InvalidListException
     * @throws InvalidSortOptionException
     * @throws \Rss\Exceptions\SortingFailedException
     */
    public function sortFeed(array $feed, string $field = 'title', bool $ascending = true)
    {
        if (!is_array($feed)) {
            return [];
        }

        if (!isset($feed['item'])) {
            return [];
        }

        return $this->sort($field, $ascending, $feed['item']);
    }

    /**
     * Temorary function to mock a feed, for testing purposes.
     */
    protected function mock()
    {
        $feed = [
            'title'         => 'The big feed',
            'lastBuildDate' => 'Tue, 29 Oct 2019 10:19:31 +0000',
            'item'          => [
                [
                    'title'     => 'Abby the Bob',
                    'pubDate'   => 'Thu, 26 Sep 2019 14:37:16 +0000',
                ],
                [
                    'title'     => 'Bobby the Cow',
                    'pubDate'   => 'Thu, 16 Sep 2019 14:37:16 +0000',
                ],
                [
                    'title'     => 'Celine the Deputy',
                    'pubDate'   => 'Thu, 29 Sep 2019 14:37:16 +0000',
                ],
            ],
        ];

        return $feed;
    }

    /**
     * @throws InvalidSortOptionException
     * @throws InvalidListException
     * @throws \Rss\Exceptions\SortingFailedException
     */
    public function sort(string $field, bool $ascending, array $list)
    {
        $this->validate($field);

        list($flat, $reverse) = $this->flatMap($field, $list);

        if ($ascending) {
            asort($flat);
        } else {
            arsort($flat);
        }

        return $this->unflatten($flat, $list, $field);
    }

    /**
     * @throws \Rss\Exceptions\SortingFailedException
     */
    protected function unflatten(array $sorted, array $list, string $field): array
    {
        $unflat = [];
        foreach ($sorted as $key => $value) {
            $element = $list[$key];
            if ($element[$field] !== $value) {
                $message = 'The sorted elements value and the value of the old list does not match!';

                throw new \Rss\Exceptions\SortingFailedException($message);
            }

            $unflat[$key] = $element;
        }

        return $unflat;
    }

    /**
     * @throws InvalidListException
     */
    protected function flatMap(string $field, array $list): array
    {
        $flat       = [];
        $reverse    = [];
        foreach ($list as $key => $value) {
            if (!is_array($value)) {
                throw new InvalidListException('The list you supplied to sorting is asymmetrical or already flat!');
            }

            if (!isset($value[$field])) {
                throw new InvalidListException('You did not supply a working list.');
            }

            $target             = $value[$field];
            $flat[$key]         = $target;
            $reverse[$target]   = $key;
        }

        return [$flat, $reverse];
    }

    /**
     * @throws InvalidSortOptionException
     */
    protected function validate(string $field): void
    {
        $allowed = [
            'title',
            'pubDate',
        ];

        if (!in_array($field, $allowed)) {
            throw new InvalidSortOptionException('Field ' . $field . ' not sortable.');
        }
    }
}
