<?php

use Core\Model\Module;

require_once 'autoloader.php';

$logger = new \Core\Model\Log();

try {
    $path       = $_SERVER['REQUEST_URI'];
    $modules    = new Module();
    $controller = $modules->matchPath($path);

    echo $controller;
} catch (\Core\Exceptions\NoRouteException $e) {
    echo '<h1>404 page not found</h1><p>Please try another route</p>';
    $logger->error($e);

} catch (\Exception $e) {
    echo '<h1>Unknown error</h1>';
    $logger->error($e);
} catch (\Error $e) {
    echo '<h1>Unknown error</h1>';
    $logger->error($e);
}
