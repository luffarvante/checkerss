<?php


namespace Remote\Model;


use Pickle\Model;
use Pickle\ObjectInterface;
use Remote\Exceptions\InvalidRssException;

class Rss implements \Pickle\ObjectInterface
{
    private $name;
    private $source;
    private $data;
    private $persistence;
    private $modified;

    public function __construct()
    {
        $this->persistence = new Model();
    }

    public function toArray(): array
    {
        return [
            'title' => $this->getName(),
            'url'   => $this->getSource()->getUrl(),
            'data'  => $this->getData(),
        ];
    }

    /*
     * Getters / Setters
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): Rss
    {
        $this->name     = $name;
        $this->modified = true;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(Source $source): Rss
    {
        $this->source   = $source;
        $this->modified = true;

        return  $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): Rss
    {
        $this->data     = $data;
        $this->modified = true;

        return $this;
    }

    public function getIsModified(): bool
    {
        if ($this->modified === null) {
            $this->modified = false;
        }

        return $this->modified;
    }

    /*
     * End of Getters / Setters
     */

    /*
     * CRUD
     */

    /**
     * @param string|null $name
     * @param Source|null $source
     * @param array|null $data
     * @return Rss
     * @throws InvalidRssException
     */
    public function create(
        ?string $name   = null,
        ?Source $source = null,
        ?array $data    = null
    ): Rss
    {
        list($name, $source) = $this->validateInput($name, $source);

        if (!$data) {
            $data = $source->readRemote($source->getUrl()); // TODO move to input
        }

        $this->setName($name)
            ->setSource($source)
            ->setData($data)
        ;

        $this->modified = false;

        return $this->persistence->create($this);
    }

    public function read(string $name): ?ObjectInterface
    {
        $type = get_class($this);

        return $this->persistence->read($name, $type);
    }

    public function update(): ?ObjectInterface
    {
        if (!$this->modified) {
            return $this;
        }

        $this->modified = false;

        return $this->persistence->update($this);
    }

    public function delete(): bool
    {
        return $this->persistence->delete($this);
    }
    /*
     * End of CRUD
     */

    /*
     * Logic
     */
    /**
     * @param string|null $name
     * @param Source|null $source
     * @return array
     * @throws InvalidRssException
     */
    protected function validateInput(?string $name, ?Source $source): array
    {
        if ($name && $source) {
            return [$name, $source];
        }

        if (!$name && $this->getName()) {
            $name = $this->getName();

            return $this->validateInput($name, $source);
        }

        if (!$source && $this->getSource()) {
            $source = $this->getSource();

            return $this->validateInput($name, $source);
        }

        if (!$name && !$source) {
            $name   = 'default';
            $source = $this->readDefaultSource();

            return [$name, $source];
        }

        if (!$name && $source && $source->getName()) {
            $name = $source->getName();

            return [$name, $source];
        }

        if ($name && $name === 'default' && !$source) {
            $source = $this->readDefaultSource();

            return [$name, $source];
        }

        throw new InvalidRssException('Name or data invalid, check data');
    }
}
