<?php


namespace Remote\Model;

use SimpleXMLElement;
use Pickle\Model;
use Pickle\ObjectInterface;
use Remote\Exceptions\InvalidSourceException;
use Remote\Model\Xml;

class Source implements \Pickle\ObjectInterface
{
    private $name;
    private $url;
    private $persistence;
    private $modified;

    public function __construct()
    {
        $this->persistence = new Model();
    }

    /*
     * Getters / Setters
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): Source
    {
        $this->name     = $name;
        $this->modified = true;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl($url): Source
    {
        $this->url      = $url;
        $this->modified = true;

        return $this;
    }

    public function getIsModified(): bool
    {
        if ($this->modified === null) {
            $this->modified = false;
        }

        return $this->modified;
    }

    /*
     * End of Getters / Setters
     */

    /*
     * CRUD
     */

    /**
     * @param string|null $name
     * @param string|null $url
     * @param bool $fallback
     * @return ObjectInterface|null
     * @throws InvalidSourceException
     */
    public function create(
        ?string $name   = null,
        ?string $url    = null,
        bool $fallback  = false
    ): ?Source
    {
        list($name, $url) = $this->validateInput($name, $url, $fallback);
        $this->setName($name)
            ->setUrl($url);

        $this->modified = false;

        return $this->persistence->create($this);
    }

    public function read(string $name): ?ObjectInterface
    {
        $type = get_class($this);

        return $this->persistence->read($name, $type);
    }

    public function readAll(): array
    {
        $type       = get_class($this);
        $sources    = $this->persistence->readAll($type);

        if (!$sources) {
            $sources = $this->createDefaultSources();
        }

        return $sources;
    }

    public function update(): ?ObjectInterface
    {
        if (!$this->modified) {
            return $this;
        }

        $this->modified = false;

        return $this->persistence->update($this);
    }

    public function delete(): bool
    {
        return $this->persistence->delete($this);
    }
    /*
     * End of CRUD
     */

    /**
     * @param string|null $name
     * @param string|null $url
     * @param bool $fallback
     * @return array
     * @throws InvalidSourceException
     */
    protected function validateInput(?string $name, ?string $url, bool $fallback)
    {
        if ($name && $url) {
            return [$name, $url];
        }

        if (!$name && $this->getName()) {
            $name = $this->getName();

            return $this->validateInput($name, $url, $fallback);
        }

        if (!$url && $this->getUrl()) {
            $url = $this->getUrl();

            return $this->validateInput($name, $url, $fallback);
        }

        $default = $this->readDefaultSource();
        if (!$url && $name === $default->getName()) {
            return [$name, $default->getUrl()];
        }

        if (!$name && $url === $default->getUrl()) {
            return [$default->getName(), $url];

        }

        if ($fallback) {
            return [$default->getName(), $default->getUrl()];
        }

        throw new InvalidSourceException('Invalid source');
    }

    /*
     * Load defaults from persistence layer
     */
    protected function readDefaultSource(): Source
    {
        $source = $this->read('default');
        if (!$source
            || !$source->getName()
            || !$source->getUrl()
        ) {
            return $this->createDefaultSource($this);
        }

        return $source;
    }

    protected function createDefaultSource(Source $source): Source
    {
        $source->setName('default');
        $source->setUrl('https://somethingpositive.net/sp.xml');
        $source = $source->create();

        return $source;
    }

    protected function createDefaultSources(): array
    {
        $sourceData = [
            'sp'        => 'https://somethingpositive.net/sp.xml',
            'jetbrains' => 'https://blog.jetbrains.com/feed/',
            'google'    => 'https://rss.app/feeds/1nPNPT9hkKGzmXWd.xml',
        ];

        $sources = [];
        foreach ($sourceData as $name => $url) {
            $source = new Source();
            $source->setName($name)->setUrl($url);

            $source     = $source->create();
            $sources[]  = $source;
        }

        return $sources;
    }

    /*
     * Read from API
     */
    public function readRemote(string $source): array
    {
        $raw        = file_get_contents($source);
        $xml        = new SimpleXMLElement($raw);
        $formatted  = $this->formatXmlData($xml);

        return $formatted;
    }

    protected function formatXmlData(SimpleXMLElement $mainXml): array
    {
        $xml            = $mainXml->children();
        $xmlFormatter   = new Xml();

        return $xmlFormatter->toArray($xml)['channel'] ?? [];
    }
}
