<?php


namespace Remote\View;


class Rss
{
    public function printText(array $feeds): void
    {
        $steps          = 0;
        foreach ($feeds as $feed) {
            $this->printSection($feed, $steps);
        }
    }

    protected function printSection($items, $steps = 0)
    {
        $indentation = $this->indent($steps);
        $steps++;

        foreach ($items as $item) {
            if (is_string($item)) {
                echo $indentation . $item . PHP_EOL;

                continue;
            }

            if (is_array($item)) {
                $this->printSection($item, $steps);

                continue;
            }
        }
    }

    protected function indent(int $steps = 0, int $size = 2)
    {
        $indentation = '';
        $increment = '';
        for ($i = 0; $i < $size; $i++) {
            $increment .= ' ';
        }

        for ($i = 0; $i < $steps; $i++) {
            $indentation .= $increment;
        }

        return $indentation;
    }

    public function presentAll(array $feeds): array
    {
        $lines = [];
        foreach ($feeds as $feed) {
            $part       = [];
            $part[]     = 'Feed name: ' . $feed['title'];
            $part[]     = 'Feed url: ' . $feed['url'];
            $part[]     = $this->present($feed['data']);
            $lines[]    = $part;
        }

        return $lines;
    }

    public function present(array $feed): array
    {
        $lines = [];
        $lines[] = 'Name: ' . $feed['title'];
        $lines[] = 'URL: ' . $feed['link'];
        $lines[] = 'Description: ' . $this->validateItem($feed['description']);

        $items = $feed['item'] ?? [];

        if (!$items) {
            $lines[] = 'No feed items.';

            return $lines;
        }

        $lines[] = 'Items:';
        $lines[] = $this->formatStep($feed['item']);

        return $lines;
    }

    protected function formatStep(array $items): array
    {
        $steps = [];
        foreach ($items as $item) {
            $subSteps = [];
            $item['description'] = $this->validateItem($item['description']);

            $subSteps[] = 'Title: ' . $item['title'];
            $subSteps[] = 'Link: ' . $item['link'];
            $subSteps[] = 'Description: ' . $item['description'];
            $steps[] = $subSteps;
        }

        return $steps;
    }

    protected function validateItem($item): string
    {
        if (is_array($item)) {
            $item = implode(', ', $item);
        }

        return $item;
    }
}