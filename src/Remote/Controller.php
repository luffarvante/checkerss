<?php


namespace Remote;


use Remote\Model\Rss;
use Remote\Model\Source;
use Rss\Model\Feed;

class Controller
{
    public function execute()
    {
        $feed           = new Feed();
        $sourceFactory  = new Source();
        $sources        = $sourceFactory->readAll();

        $feeds = [];
        foreach ($sources as $source) {
            $rss = new Rss();
            $rss = $rss->create(null, $source, null); // Create basic RSS based on source

            $rss->setData($feed->sortFeed($rss->getData()));

            $feeds[] = $rss->toArray();
        }

        return json_encode($feeds);
    }
}
