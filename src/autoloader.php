<?php

/**
 * Not actually an autoloader, but lets pretend it is.
 */
require_once 'Pickle/Model.php';
require_once 'Pickle/ObjectInterface.php';

require_once 'Remote/Model/Rss.php';
require_once 'Remote/Model/Source.php';
require_once 'Remote/Model/Xml.php';
require_once 'Remote/Controller.php';
require_once 'Remote/View/Rss.php';

require_once 'Core/Model/Module.php';
require_once 'Core/Model/Log.php';
require_once 'Core/Exceptions/NoRouteException.php';

require_once 'Rss/Model/Feed.php';
