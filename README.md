# Description
An RSS reader with a small prototype MVC framework attached.
The name “CheckeRSS” means “Checker of RSS” (the ‘of’ is silent).
Yes, I know that’s not funny.

## Project Requirements
* Fetch RSS 2.0 feeds from a minimum of 2 sources
* Run from the command line
* Present result in either text or HTML
* Sort results either by date or title
* Handle Exceptions / Errors
* Handle all required RSS fields

### Optional
* Add further output formats if the need arises
* Ability to add more RSS sources


I interpreted the description of the task as requiring both browser and CLI functionality but realized later that that 
might have been a misinterpretation.
Therefore the browser functionality priority was lowered.

## Status
#### Fetch RSS 2.0 feeds from a minimum of 2 sources
Solved, the project has 3 default sources. I noticed the contents of the feeds retrieved were inconsistent though,
so there is room for improvement.

#### Run from the command line
Solved

#### Present result in either text or HTML
Command line results are presented in a very basic text formatting. I’ve implemented it in a way that will enable also 
expanding with HTML formatting if desired with just minor adjustments.

#### Sort results either by date or title
The backend supports sorting by both date and title, as well as ordering by ascending and descending.
User input was never added, so to adjust sorting the code needs to be modified.
To modfiy this, go to `./Remote/Controller.php and add parameters to sortFeed() on line 24`

#### Handle Exceptions / Errors
Very basic implementation where errors are caught at the top level (`index.php` and `run.php`) and logs to file.
The logging is implemented in a dynamic way so this could easily be expanded.

#### Handle all required RSS fields
Works with the JetBrains feed (one of the 3 defaults) but either I messed something up or the other two default 
feeds are not following specifications.

### Optional

#### Add further output formats if the need arises
Done, any other output format could be added fairly easy due to the general formatting of the data along with the MVC 
framework.

#### Ability to add more RSS sources
Cannot be done dynamically by user, but adding a new default source can be done by adding a name and source at 
`./Remote/Model/Source.php line 199`.
Creating a CLI command to add a new source, or a form for the browser (if you implement the HTML view for it first) 
should be a matter of 1-2 hours maximum, with testing.

### Other
The browser implementation only displays the json from the controller; the intent was to add a view to add HTML 
formatting tags to the output.

## About the framework
I opted to not use an existing framework as it felt unnecessary for the scope of the project.
Originally I planned to just make a very simple MVC-like setup but got a bit carried away; it felt like the other 
project requirements would be easier to implement on a stable foundation.

The framework is designed to work with both CLI and browser functionality.
Currently the browser view is missing but there is a very basic one for the CLI that lists the RSS feed with some 
indentation. The browser just displays the json returned by the controller.

The CLI script uses the Rss controller directly, while index.php loads it dynamically from the URI. It looks for a
Controller file in the namespace matching the URI.


By default the project will generate three default RSS sources and managing classes for them, and save for future use
(they are only generated if they don't already exist).

### Persistence
I implemented a very basic persistence module which is essentially a clone of Python's pickle package.
It serializes and deserializes classes and saves / loads to text files, structured by module namespace.

The idea was that this would allow me to dynamically add and remove RSS sources.

## Conclusion
I think i built a rather stable and scalable foundation but my priorities were definitely wrong.
I should have ensured I fulfilled all basic requirements of the project *before* building things that were out of scope.
I think the basic idea was still not entirely wrong and if I had the amount of time I originally thought I had 
(some private matters came up that, unfortunately, consumed some time that was originally allocated to the project)
I believe it would have been in a more complete state in the end while still having the basic framework it has today.
**But I should have adjusted the priority of all tasks as soon as I realized the available time was reduced**.
I will definitely take this experience with me and try to learn from it.

All in all the project was really fun, my only regret is that I handled the priority of the task poorly and that 
I “lost” time I was planning to use for the project.
It was a fun challenge to try to write my own little framework (even though that was outside the scope of the project, 
I know) and I think I learned a lot about PHP and framework structure when it was broken down and this lightweight, 
when working in Magento you never really see those details due to the sheer amount of code.
I do plan to continue developing this as a hobby project as it feels a bit wrong to leave it unfinished.

# Setup and usage
To setup the project, clone it using git:
```bash
git pull https://gitlab.com/luffarvante/checkerss.git
```

The project requires PHP installed on the machine to work and was written for PHP 7.2 syntax.
It was designed to work with PHP’s built in webserver for testing purposes.

All commands assume you are in the <project folder>/src unless stated otherwise.

## CLI
`php bin/run.php`

This will either read the RSS feeds in your resources folder or create them from defaults and then read them, 
if they do not exist.

## Browser
Start the php built in webserver
```bash
Php -S localhost:8000
```

Navigate to http://localhost:8000/remote in your browser.

You will be given a string containing the formatted RSS json.

The source is the same as for the CLI script and is handled the same way;
the only difference is that the browser loads the controller dynamically from your base url parameters.

# Code standard
I’ve followed the [PSR-12 code standard](https://www.php-fig.org/psr/psr-12/) with a few modifications.
The most notably of those is that I align all variable declarations and array values to increase readability.

## PHPDoc
You may notice the PHPDoc is rather sparse.
This is intentional, as I've tried to write code that is very easy to understand.
The PHPDoc comments are mainly used to declare if a method may throw an Error or Exception, 
so unless that could happen in the method it does not have a PHPDoc comment.
